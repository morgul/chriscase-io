#-----------------------------------------------------------------------------------------------------------------------
# Bundle Stage - Do all our bundling of assets
#-----------------------------------------------------------------------------------------------------------------------

FROM node:4-slim as bundle-builder

RUN mkdir -p /app
WORKDIR /app

ADD . /app/

RUN npm install
RUN npm run build

#-----------------------------------------------------------------------------------------------------------------------
# NPM Stage - Install production packages and clean cache
#-----------------------------------------------------------------------------------------------------------------------

FROM node:4-slim as npm-builder

COPY --from=bundle-builder /app /app

WORKDIR /app

RUN npm install --production

#-----------------------------------------------------------------------------------------------------------------------
# Final Docker
#-----------------------------------------------------------------------------------------------------------------------

FROM node:4-slim
EXPOSE 5678

# Only copy the files we actually need
COPY --from=bundle-builder /app/client /app/client
COPY --from=npm-builder /app/node_modules /app/node_modules
COPY --from=bundle-builder /app/package.json /app/

# Make DB directory
RUN mkdir -p /app/server/db

# Make uploads directory
RUN mkdir -p  /app/uploads

WORKDIR /app
ADD ./server /app/server
ADD ./server.js /app/server.js
ADD ./config.js /app/config.js

VOLUME /app/db

EXPOSE 4568

CMD [ "npm", "start",  "# chriscase.io" ]

#-----------------------------------------------------------------------------------------------------------------------

